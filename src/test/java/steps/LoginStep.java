package steps;

import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;
import java.util.Map;

public class LoginStep {

    @Before
    public void setUp(){
        System.out.println("SetUp the application");
    }

    @After
    public void tearDown() {
        System.out.println("Finish the web browsers");
    }

    @Before("@second_example")
    public void setUpSecond(){
        System.out.println("SetUp the second application");
    }

    @After("@second_example")
    public void tearDownSecond() {
        System.out.println("Finish the second web browsers");
    }

    @Given("^user navigates do facebook website$")
    public void user_navigates_do_facebook_website() throws Throwable {
        System.out.print("@Given(\"^user navigates do facebook website$\")");
    }

    @When("^user validates the homepage title$")
    public void user_validates_the_homepage_title() throws Throwable {
        System.out.print("^user validates the homepage title$");
    }

    @When("^user entered \"([^\"]*)\" username$")
    public void user_entered_the_username(String username) throws Throwable {
        System.out.print("^user entered " + username + " username$");
    }

    @And("^user entered \"([^\"]*)\" password$")
    public void user_entered_the_password(String password) throws Throwable {
        System.out.print("^user entered " + password + " password$");
    }

    @And("^user select the age category$")
    public void user_select_the_age_category(List<String> list) throws Throwable {
        System.out.print("^user select the age category: $" + list.get(1));
    }

    @And("^user select the sex and the location$")
    public void user_select_the_sex_and_the_location(DataTable table) throws Throwable {
        List<Map<String, String>> data = table.asMaps(String.class, String.class);
        System.out.println("^user select the sex and the location: " + data.get(1).get("Sex") + " - Location as : " + data.get(1).get("Location"));
    }

    @Then("^user \"([^\"]*)\" successfully logged in$")
    public void user_should_be_sucessfully_logged_in(String validateLogin) throws Throwable {
        System.out.print("^user " + validateLogin + " sucessfully logged in$");
    }

}
