@second_example @third_example
Feature: Second Login

  In order to perform successful the second Login
  As a User
  I want to enter correct username and password

Background:
  Given user navigates do facebook website
  When user validates the homepage title

Scenario Outline: In order to verify login to Facebook
  When user entered "<username>" username
  And user entered "<password>" password
  And user select the age category
  | Age      |
  | below 18 |
  | above 18 |
  And user select the sex and the location
  | Sex    | Location |
  | Male   | Brazil   |
  | Female | USA      |
  Then user "<loginType>" successfully logged in

Examples:
  | username | password | loginType |
  | valid    | valid    | should    |