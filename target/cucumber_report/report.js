$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Login.feature");
formatter.feature({
  "line": 2,
  "name": "Login",
  "description": "\r\nIn order to perform successful Login\r\nAs a User\r\nI want to enter correct username and password",
  "id": "login",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@first_example"
    }
  ]
});
formatter.scenarioOutline({
  "line": 12,
  "name": "In order to verify login to Facebook",
  "description": "",
  "id": "login;in-order-to-verify-login-to-facebook",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 13,
  "name": "user entered \"\u003cusername\u003e\" username",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "user entered \"\u003cpassword\u003e\" password",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user select the age category",
  "rows": [
    {
      "cells": [
        "Age"
      ],
      "line": 16
    },
    {
      "cells": [
        "below 18"
      ],
      "line": 17
    },
    {
      "cells": [
        "above 18"
      ],
      "line": 18
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "user select the sex and the location",
  "rows": [
    {
      "cells": [
        "Sex",
        "Location"
      ],
      "line": 20
    },
    {
      "cells": [
        "Male",
        "Brazil"
      ],
      "line": 21
    },
    {
      "cells": [
        "Female",
        "USA"
      ],
      "line": 22
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "user \"\u003cloginType\u003e\" successfully logged in",
  "keyword": "Then "
});
formatter.examples({
  "line": 25,
  "name": "",
  "description": "",
  "id": "login;in-order-to-verify-login-to-facebook;",
  "rows": [
    {
      "cells": [
        "username",
        "password",
        "loginType"
      ],
      "line": 26,
      "id": "login;in-order-to-verify-login-to-facebook;;1"
    },
    {
      "cells": [
        "valid",
        "valid",
        "should"
      ],
      "line": 27,
      "id": "login;in-order-to-verify-login-to-facebook;;2"
    },
    {
      "cells": [
        "invalid",
        "invalid",
        "shouldnt"
      ],
      "line": 28,
      "id": "login;in-order-to-verify-login-to-facebook;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 293798,
  "status": "passed"
});
formatter.background({
  "line": 8,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 9,
  "name": "user navigates do facebook website",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "user validates the homepage title",
  "keyword": "When "
});
formatter.match({
  "location": "LoginStep.user_navigates_do_facebook_website()"
});
formatter.result({
  "duration": 191777236,
  "status": "passed"
});
formatter.match({
  "location": "LoginStep.user_validates_the_homepage_title()"
});
formatter.result({
  "duration": 65003,
  "status": "passed"
});
formatter.scenario({
  "line": 27,
  "name": "In order to verify login to Facebook",
  "description": "",
  "id": "login;in-order-to-verify-login-to-facebook;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@first_example"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "user entered \"valid\" username",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "user entered \"valid\" password",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user select the age category",
  "rows": [
    {
      "cells": [
        "Age"
      ],
      "line": 16
    },
    {
      "cells": [
        "below 18"
      ],
      "line": 17
    },
    {
      "cells": [
        "above 18"
      ],
      "line": 18
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "user select the sex and the location",
  "rows": [
    {
      "cells": [
        "Sex",
        "Location"
      ],
      "line": 20
    },
    {
      "cells": [
        "Male",
        "Brazil"
      ],
      "line": 21
    },
    {
      "cells": [
        "Female",
        "USA"
      ],
      "line": 22
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "user \"should\" successfully logged in",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "valid",
      "offset": 14
    }
  ],
  "location": "LoginStep.user_entered_the_username(String)"
});
formatter.result({
  "duration": 5952946,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "valid",
      "offset": 14
    }
  ],
  "location": "LoginStep.user_entered_the_password(String)"
});
formatter.result({
  "duration": 163792,
  "status": "passed"
});
formatter.match({
  "location": "LoginStep.user_select_the_age_category(String\u003e)"
});
formatter.result({
  "duration": 1635349,
  "status": "passed"
});
formatter.match({
  "location": "LoginStep.user_select_the_sex_and_the_location(DataTable)"
});
formatter.result({
  "duration": 1733282,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "should",
      "offset": 6
    }
  ],
  "location": "LoginStep.user_should_be_sucessfully_logged_in(String)"
});
formatter.result({
  "duration": 165930,
  "status": "passed"
});
formatter.after({
  "duration": 67142,
  "status": "passed"
});
formatter.before({
  "duration": 106913,
  "status": "passed"
});
formatter.background({
  "line": 8,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 9,
  "name": "user navigates do facebook website",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "user validates the homepage title",
  "keyword": "When "
});
formatter.match({
  "location": "LoginStep.user_navigates_do_facebook_website()"
});
formatter.result({
  "duration": 108625,
  "status": "passed"
});
formatter.match({
  "location": "LoginStep.user_validates_the_homepage_title()"
});
formatter.result({
  "duration": 54740,
  "status": "passed"
});
formatter.scenario({
  "line": 28,
  "name": "In order to verify login to Facebook",
  "description": "",
  "id": "login;in-order-to-verify-login-to-facebook;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@first_example"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "user entered \"invalid\" username",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "user entered \"invalid\" password",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user select the age category",
  "rows": [
    {
      "cells": [
        "Age"
      ],
      "line": 16
    },
    {
      "cells": [
        "below 18"
      ],
      "line": 17
    },
    {
      "cells": [
        "above 18"
      ],
      "line": 18
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "user select the sex and the location",
  "rows": [
    {
      "cells": [
        "Sex",
        "Location"
      ],
      "line": 20
    },
    {
      "cells": [
        "Male",
        "Brazil"
      ],
      "line": 21
    },
    {
      "cells": [
        "Female",
        "USA"
      ],
      "line": 22
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "user \"shouldnt\" successfully logged in",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "invalid",
      "offset": 14
    }
  ],
  "location": "LoginStep.user_entered_the_username(String)"
});
formatter.result({
  "duration": 159088,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "invalid",
      "offset": 14
    }
  ],
  "location": "LoginStep.user_entered_the_password(String)"
});
formatter.result({
  "duration": 188168,
  "status": "passed"
});
formatter.match({
  "location": "LoginStep.user_select_the_age_category(String\u003e)"
});
formatter.result({
  "duration": 166785,
  "status": "passed"
});
formatter.match({
  "location": "LoginStep.user_select_the_sex_and_the_location(DataTable)"
});
formatter.result({
  "duration": 272416,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "shouldnt",
      "offset": 6
    }
  ],
  "location": "LoginStep.user_should_be_sucessfully_logged_in(String)"
});
formatter.result({
  "duration": 130007,
  "status": "passed"
});
formatter.after({
  "duration": 91090,
  "status": "passed"
});
formatter.uri("SecondLogin.feature");
formatter.feature({
  "line": 2,
  "name": "Second Login",
  "description": "\r\nIn order to perform successful the second Login\r\nAs a User\r\nI want to enter correct username and password",
  "id": "second-login",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@second_example"
    },
    {
      "line": 1,
      "name": "@third_example"
    }
  ]
});
formatter.scenarioOutline({
  "line": 12,
  "name": "In order to verify login to Facebook",
  "description": "",
  "id": "second-login;in-order-to-verify-login-to-facebook",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 13,
  "name": "user entered \"\u003cusername\u003e\" username",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "user entered \"\u003cpassword\u003e\" password",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user select the age category",
  "rows": [
    {
      "cells": [
        "Age"
      ],
      "line": 16
    },
    {
      "cells": [
        "below 18"
      ],
      "line": 17
    },
    {
      "cells": [
        "above 18"
      ],
      "line": 18
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "user select the sex and the location",
  "rows": [
    {
      "cells": [
        "Sex",
        "Location"
      ],
      "line": 20
    },
    {
      "cells": [
        "Male",
        "Brazil"
      ],
      "line": 21
    },
    {
      "cells": [
        "Female",
        "USA"
      ],
      "line": 22
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "user \"\u003cloginType\u003e\" successfully logged in",
  "keyword": "Then "
});
formatter.examples({
  "line": 25,
  "name": "",
  "description": "",
  "id": "second-login;in-order-to-verify-login-to-facebook;",
  "rows": [
    {
      "cells": [
        "username",
        "password",
        "loginType"
      ],
      "line": 26,
      "id": "second-login;in-order-to-verify-login-to-facebook;;1"
    },
    {
      "cells": [
        "valid",
        "valid",
        "should"
      ],
      "line": 27,
      "id": "second-login;in-order-to-verify-login-to-facebook;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 82109,
  "status": "passed"
});
formatter.before({
  "duration": 117177,
  "status": "passed"
});
formatter.background({
  "line": 8,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 9,
  "name": "user navigates do facebook website",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "user validates the homepage title",
  "keyword": "When "
});
formatter.match({
  "location": "LoginStep.user_navigates_do_facebook_website()"
});
formatter.result({
  "duration": 68853,
  "status": "passed"
});
formatter.match({
  "location": "LoginStep.user_validates_the_homepage_title()"
});
formatter.result({
  "duration": 61154,
  "status": "passed"
});
formatter.scenario({
  "line": 27,
  "name": "In order to verify login to Facebook",
  "description": "",
  "id": "second-login;in-order-to-verify-login-to-facebook;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@second_example"
    },
    {
      "line": 1,
      "name": "@third_example"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "user entered \"valid\" username",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "user entered \"valid\" password",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user select the age category",
  "rows": [
    {
      "cells": [
        "Age"
      ],
      "line": 16
    },
    {
      "cells": [
        "below 18"
      ],
      "line": 17
    },
    {
      "cells": [
        "above 18"
      ],
      "line": 18
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "user select the sex and the location",
  "rows": [
    {
      "cells": [
        "Sex",
        "Location"
      ],
      "line": 20
    },
    {
      "cells": [
        "Male",
        "Brazil"
      ],
      "line": 21
    },
    {
      "cells": [
        "Female",
        "USA"
      ],
      "line": 22
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "user \"should\" successfully logged in",
  "matchedColumns": [
    2
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "valid",
      "offset": 14
    }
  ],
  "location": "LoginStep.user_entered_the_username(String)"
});
formatter.result({
  "duration": 144547,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "valid",
      "offset": 14
    }
  ],
  "location": "LoginStep.user_entered_the_password(String)"
});
formatter.result({
  "duration": 127869,
  "status": "passed"
});
formatter.match({
  "location": "LoginStep.user_select_the_age_category(String\u003e)"
});
formatter.result({
  "duration": 113328,
  "status": "passed"
});
formatter.match({
  "location": "LoginStep.user_select_the_sex_and_the_location(DataTable)"
});
formatter.result({
  "duration": 236921,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "should",
      "offset": 6
    }
  ],
  "location": "LoginStep.user_should_be_sucessfully_logged_in(String)"
});
formatter.result({
  "duration": 150962,
  "status": "passed"
});
formatter.after({
  "duration": 53029,
  "status": "passed"
});
formatter.after({
  "duration": 106913,
  "status": "passed"
});
});