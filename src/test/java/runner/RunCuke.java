package runner;

import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

import java.io.File;

@RunWith(Cucumber.class)
@CucumberOptions(features={"src/test/java/features"},
        glue={"steps"},
        monochrome = true,
        tags = {"@first_example, @second_example"}, //responsável por selecionar quais features serão executadas
        plugin = {"com.cucumber.listener.ExtentCucumberFormatter:target/new_cucumber_report.html"})
        //plugin = {"pretty", "html:target/cucumber_report"}) // report básico do Cucumber
public class RunCuke {

    @AfterClass
    public static void setup() {
        Reporter.loadXMLConfig(new File("src/test/resources/extent-config.xml"));
        Reporter.setSystemInfo("user", System.getProperty("user.name"));
        Reporter.setSystemInfo("os", "Windows");
        Reporter.setTestRunnerOutput("Sample test runner output message");
    }
}
